#!/usr/bin/env python3

# ---------------------------
# projects/collatz/Collatz.py
# Copyright (C)
# Glenn P. Downing
# ---------------------------

# ------------
# collatz_read
# ------------

cvalues = {
    1: 1,
    2: 2,
    5: 8,
    6: 9,
    8: 17,
    17: 20,
    18: 21,
    24: 21,
    26: 24,
    53: 112,
    54: 113,
    72: 113,
    96: 116,
    128: 119,
    170: 122,
    230: 125,
    234: 128,
    312: 128,
    326: 131,
    648: 144,
    653: 145,
    654: 145,
    666: 145,
    702: 145,
    870: 171,
    1160: 179,
    2222: 182,
    2321: 183,
    2322: 183,
    2462: 183,
    2918: 209,
    3710: 217,
    6170: 238,
    10970: 262,
    13254: 268,
    17646: 276,
    17672: 279,
    23528: 279,
    26622: 282,
    34238: 308,
    35496: 311,
    35654: 311,
    52526: 324,
    77030: 340,
    106238: 351,
    142586: 354,
    156158: 375,
    216366: 383,
    230630: 386,
    410010: 443,
    511934: 449,
    626330: 470,
    837798: 509
}


def collatz_read(s):
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]



# ------------s
# collatz_eval
# ------------


def collatz_eval(i, j):
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert i > 0 #argument validity
    assert j > 0 #argument validity
    if( j < i):
        temp = i
        i = j
        j = temp
    r=[]
    val =[]
    idx = 0
    for c in range(j, i-1, -1):
        if c in cvalues:
            idx = (list(cvalues.keys()).index(c))
        if idx > 0:
            rs = list(cvalues.keys())
            vals = list(cvalues.values())
            val = [vals[idx + 1] ]
            r = [rs[idx -1]]
            break
    try:
        if (i < r[0]):
            return(val[0])
    except: 
        lens = []
        for c in range(i,j+1):
            length = 1
            while c != 1:
                if c % 2 == 0:
                    c = c/2
                else:
                    c = 3*c +1
                length = length + 1
            lens.append(length)
        assert max(lens) > 0 #return validity
        return max(lens)

# -------------
# collatz_print
# -------------


def collatz_print(w, i, j, v):
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")

# -------------
# collatz_solve
# -------------


def collatz_solve(r, w):
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
